import re
import sys
class MyInterpreter:
    """
    Interpreter for computer architecture course assembly
    """
    def __init__(self):
        if len(sys.argv) > 2:
            fname = sys.argv[1]
            self.__output_file_name = sys.argv[2]
        else:
            self.__output_file_name = './output/output.txt'
            if len(sys.argv) > 1:
                fname = sys.argv[1]
            else:
                fname = 'input/test.txt'
        with open(fname, encoding='utf-8') as file:
            self.src = file.readlines()
        self.__token_table = []
        self.__labels = []
        self.__function = []
        self.__hex = []
        self.__bin = []
        self.__hex_string = []
        self.__bin_string = []
        self.keyword = ['DEC', 'HEX', 'ORG', 'AND', 'ADD', 'LDA', 'STA', 'BUN', 'BSA', 'ISZ', 'CLA',
                        'CLE', 'CMA', 'CME', 'CIR', 'CIL', 'INC', 'SPA', 'SNA', 'SZA', 'SZE', 'HLT',
                        'INP', 'OUT', 'SKI', 'SKO', 'ION', 'IOF']
        self.__lc = 0
    def __enter__(self):
        return self
    def __exit__(self, exc_type, exc_value, exc_traceback):
        if exc_type is not None and exc_type is not KeyboardInterrupt:
            msg = 'exc_type= '+str(exc_type)+'\n'
            msg += 'exc_value= '+str(exc_value)+'\n'
            #traceback.print_tb(exc_traceback)
            print(msg)
        '''if exc_type in KeyboardInterrupt:
            return False
        return True'''

    def token_table(self):
        """
        Make Token Table
        """
        reg = r"\s*(\w+)\s*,\s*(DEC|HEX)\s+(\d+|[0-9a-f]+)"
        reg2 = r"\s*ORG\s+([0-9a-f]{1,3})"
        reg3 = r"\s*(\w+)\s*:"
        reg4 = r"\s*(\w+)\s*,\s*\n"
        pattern = re.compile(reg)
        pattern2 = re.compile(reg2)
        pattern3 = re.compile(reg3)
        pattern4 = re.compile(reg4)
        for i in self.src:
            rans = re.findall(pattern, i)
            rans2 = re.findall(pattern2, i)
            if len(rans2):
                self.__lc = int(rans2[0], 16)
            elif len(rans):
                rans = rans[0]
                for j in self.__token_table:
                    if j['name'] == rans[0]:
                        raise Exception('Duplicate variable name:\n'+rans[0])
                self.__token_table.append({'name': rans[0], 'type': rans[1], 'value': rans[2],
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            else:
                self.__lc += 1
        self.__lc = 0
        for i in self.src:
            rans2 = re.findall(pattern2, i)
            rans3 = re.findall(pattern3, i)
            rans4 = re.findall(pattern4, i)
            if len(rans3):
                for j in self.__labels:
                    if j['name'] == rans3[0]:
                        raise Exception('Duplicate label name:\n'+rans3[0])
                self.__labels.append({'name': rans3[0],
                                      'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            elif len(rans2):
                self.__lc = int(rans2[0], 16)
            elif len(rans4):
                for j in self.__function:
                    if j['name'] == rans4[0]:
                        raise Exception('Duplicate function name:\n'+rans4[0])
                self.__function.append({'name': rans4[0],
                                        'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            else:
                self.__lc += 1
        self.__lc = 0
        for i in self.__function:
            for j in self.__labels:
                if i['name'] == j['name']:
                    raise Exception('Same name in __function and __labels:\n'+i['name'])
    def txt_to_hex(self):
        """
        Convert  text  to  hexdecimal
        """
        for i in self.src:
            key = self.find_keyword(i)
            if key == 'HEX':
                reg = r'\s*\w+\s*,\s*HEX\s+([0-9a-f]{1,4})'
                pattern = re.compile(reg)
                rans = re.findall(pattern, i)
                self.__bin.append({'code': str(bin(int(rans[0], 16))[2:].zfill(16)),
                                   'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            elif key == 'DEC':
                reg = r'\s*\w+\s*,\s*DEC\s+(\d+)'
                pattern = re.compile(reg)
                rans = re.findall(pattern, i)
                self.__bin.append({'code': str(bin(int(rans[0]))[2:].zfill(16)),
                                   'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            elif key == 'ORG':
                reg = r'\s*ORG\s+([0-9a-f]{1,3})'
                pattern = re.compile(reg)
                rans = re.findall(pattern, i)
                self.__lc = int(rans[0], 16)
            elif key == 'AND':
                reg1 = r'\s*AND\s+(\w+)\s*\n'
                reg2 = r'\s*I\s+AND\s+(\w+)\s*\n'
                pattern1 = re.compile(reg1)
                pattern2 = re.compile(reg2)
                rans1 = re.findall(pattern1, i)
                rans2 = re.findall(pattern2, i)
                flag = False
                address = ''
                for j in self.__token_table:
                    if len(rans1):
                        if j['name'] == rans1[0]:
                            flag = True
                            address = j['address']
                    elif len(rans2):
                        if j['name'] == rans2[0]:
                            flag = True
                            address = j['address']
                if flag:
                    if len(rans2):
                        self.__bin.append({'code': '1000' + address,
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    elif len(rans1):
                        self.__bin.append({'code': '0000' + address,
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    else:
                        raise Exception('Error in line:\n'+i)
                else:
                    if len(rans2):
                        self.__bin.append({'code': '1000'+str(bin(int(rans2[0], 16))[2:].zfill(12)),
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    elif len(rans1):
                        self.__bin.append({'code': '0000'+str(bin(int(rans1[0], 16))[2:].zfill(12)),
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    else:
                        raise Exception('Error in line:\n'+i)
                self.__lc += 1
            elif key == 'ADD':
                reg1 = r'\s*ADD\s+(\w+)\s*\n'
                reg2 = r'\s*I\s+ADD\s+(\w+)\s*\n'
                pattern1 = re.compile(reg1)
                pattern2 = re.compile(reg2)
                rans1 = re.findall(pattern1, i)
                rans2 = re.findall(pattern2, i)
                flag = False
                address = ''
                for j in self.__token_table:
                    if len(rans1):
                        if j['name'] == rans1[0]:
                            flag = True
                            address = j['address']
                    elif len(rans2):
                        if j['name'] == rans2[0]:
                            flag = True
                            address = j['address']
                if flag:
                    if len(rans2):
                        self.__bin.append({'code': '1001' + address,
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    elif len(rans1):
                        self.__bin.append({'code': '0001' + address,
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    else:
                        raise Exception('Error in line:\n'+i)
                else:
                    if len(rans2):
                        self.__bin.append({'code': '1001'+str(bin(int(rans2[0], 16))[2:].zfill(12)),
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    elif len(rans1):
                        self.__bin.append({'code': '0001'+str(bin(int(rans1[0], 16))[2:].zfill(12)),
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    else:
                        print(rans1)
                        print(rans2)
                        raise Exception('Error in line:\n'+i)
                self.__lc += 1
            elif key == 'LDA':
                reg1 = r'\s*LDA\s+(\w+)\s*\n'
                reg2 = r'\s*I\s+LDA\s+(\w+)\s*\n'
                pattern1 = re.compile(reg1)
                pattern2 = re.compile(reg2)
                rans1 = re.findall(pattern1, i)
                rans2 = re.findall(pattern2, i)
                flag = False
                address = ''
                for j in self.__token_table:
                    if len(rans1):
                        if j['name'] == rans1[0]:
                            flag = True
                            address = j['address']
                    elif len(rans2):
                        if j['name'] == rans2[0]:
                            flag = True
                            address = j['address']
                if flag:
                    if len(rans2):
                        self.__bin.append({'code': '1010' + address,
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    elif len(rans1):
                        self.__bin.append({'code': '0010' + address,
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    else:
                        raise Exception('Error in line:\n'+i)
                else:
                    if len(rans2):
                        self.__bin.append({'code': '1010'+str(bin(int(rans2[0], 16))[2:].zfill(12)),
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    elif len(rans1):
                        self.__bin.append({'code': '0010'+str(bin(int(rans1[0], 16))[2:].zfill(12)),
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    else:
                        print(rans1)
                        print(rans2)
                        raise Exception('Error in line:\n'+i)
                self.__lc += 1
            elif key == 'STA':
                reg1 = r'\s*STA\s+(\w+)\s*\n'
                reg2 = r'\s*I\s+STA\s+(\w+)\s*\n'
                pattern1 = re.compile(reg1)
                pattern2 = re.compile(reg2)
                rans1 = re.findall(pattern1, i)
                rans2 = re.findall(pattern2, i)
                flag = False
                address = ''
                for j in self.__token_table:
                    if len(rans1):
                        if j['name'] == rans1[0]:
                            flag = True
                            address = j['address']
                    elif len(rans2):
                        if j['name'] == rans2[0]:
                            flag = True
                            address = j['address']
                if flag:
                    if len(rans2):
                        self.__bin.append({'code': '1011' + address,
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    elif len(rans1):
                        self.__bin.append({'code': '0011' + address,
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    else:
                        raise Exception('Error in line:\n'+i)
                else:
                    if len(rans2):
                        self.__bin.append({'code': '1011'+str(bin(int(rans2[0], 16))[2:].zfill(12)),
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    elif len(rans1):
                        self.__bin.append({'code': '0011'+str(bin(int(rans1[0], 16))[2:].zfill(12)),
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    else:
                        print(rans1)
                        print(rans2)
                        raise Exception('Error in line:\n'+i)
                self.__lc += 1
            elif key == 'BUN':
                reg1 = r'\s*BUN\s+(\w+)\s*\n'
                reg2 = r'\s*I\s+BUN\s+(\w+)\s*\n'
                pattern1 = re.compile(reg1)
                pattern2 = re.compile(reg2)
                rans1 = re.findall(pattern1, i)
                rans2 = re.findall(pattern2, i)
                flag1 = False
                flag2 = False
                address = ''
                for j in self.__token_table:
                    if len(rans1):
                        if j['name'] == rans1[0]:
                            flag1 = True
                            address_t = j['address']
                    elif len(rans2):
                        if j['name'] == rans2[0]:
                            flag1 = True
                            address_t = j['address']
                for j in self.__function:
                    if len(rans1):
                        if j['name'] == rans1[0]:
                            flag2 = True
                            address_l = j['address']
                    elif len(rans2):
                        if j['name'] == rans2[0]:
                            flag2 = True
                            address_l = j['address']
                if flag1:
                    if len(rans2):
                        self.__bin.append({'code': '1100' + address_t,
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    elif len(rans1):
                        self.__bin.append({'code': '0100' + address_t,
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    else:
                        raise Exception('Error in line:\n'+i)
                elif flag2:
                    if len(rans2):
                        self.__bin.append({'code': '1100' + address_l,
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    elif len(rans1):
                        self.__bin.append({'code': '0100' + address_l,
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    else:
                        raise Exception('Error in line:\n'+i)
                else:
                    if len(rans2):
                        self.__bin.append({'code': '1100'+str(bin(int(rans2[0], 16))[2:].zfill(12)),
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    elif len(rans1):
                        self.__bin.append({'code': '0100'+str(bin(int(rans1[0], 16))[2:].zfill(12)),
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    else:
                        print(rans1)
                        print(rans2)
                        raise Exception('Error in line:\n'+i)
                self.__lc += 1
            elif key == 'BSA':
                reg1 = r'\s*BSA\s+(\w+)\s*\n'
                reg2 = r'\s*I\s+BSA\s+(\w+)\s*\n'
                pattern1 = re.compile(reg1)
                pattern2 = re.compile(reg2)
                rans1 = re.findall(pattern1, i)
                rans2 = re.findall(pattern2, i)
                flag = False
                address = ''
                for j in self.__token_table:
                    if len(rans1):
                        if j['name'] == rans1[0]:
                            flag = True
                            address = j['address']
                    elif len(rans2):
                        if j['name'] == rans2[0]:
                            flag = True
                            address = j['address']
                if flag:
                    if len(rans2):
                        self.__bin.append({'code': '1101' + address,
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    elif len(rans1):
                        self.__bin.append({'code': '0101' + address,
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    else:
                        raise Exception('Error in line:\n'+i)
                else:
                    if len(rans2):
                        self.__bin.append({'code': '1101'+str(bin(int(rans2[0], 16))[2:].zfill(12)),
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    elif len(rans1):
                        self.__bin.append({'code': '0101'+str(bin(int(rans1[0], 16))[2:].zfill(12)),
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    else:
                        print(rans1)
                        print(rans2)
                        raise Exception('Error in line:\n'+i)
                self.__lc += 1
            elif key == 'ISZ':
                reg1 = r'\s*ISZ\s+(\w+)\s*\n'
                reg2 = r'\s*I\s+ISZ\s+(\w+)\s*\n'
                pattern1 = re.compile(reg1)
                pattern2 = re.compile(reg2)
                rans1 = re.findall(pattern1, i)
                rans2 = re.findall(pattern2, i)
                flag = False
                address = ''
                for j in self.__token_table:
                    if len(rans1):
                        if j['name'] == rans1[0]:
                            flag = True
                            address = j['address']
                    elif len(rans2):
                        if j['name'] == rans2[0]:
                            flag = True
                            address = j['address']
                if flag:
                    if len(rans2):
                        self.__bin.append({'code': '1110' + address,
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    elif  len(rans1):
                        self.__bin.append({'code': '0110' + address,
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    else:
                        raise Exception('Error in line:\n'+i)
                else:
                    if len(rans2):
                        self.__bin.append({'code': '1110'+str(bin(int(rans2[0], 16))[2:].zfill(12)),
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    elif len(rans1):
                        self.__bin.append({'code': '0110'+str(bin(int(rans1[0], 16))[2:].zfill(12)),
                                           'address': str(bin(self.__lc)[2:].zfill(12))})
                    else:
                        print(rans1)
                        print(rans2)
                        raise Exception('Error in line:\n'+i)
                self.__lc += 1
            elif key == 'CLA':
                self.__bin.append({'code': '0111100000000000',
                                   'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            elif key == 'CLE':
                self.__bin.append({'code': '0111010000000000',
                                   'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            elif key == 'CMA':
                self.__bin.append({'code': '0111001000000000',
                                   'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            elif key == 'CME':
                self.__bin.append({'code': '0111000100000000',
                                   'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            elif key == 'CIR':
                self.__bin.append({'code': '0111000010000000',
                                   'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            elif key == 'CIL':
                self.__bin.append({'code': '0111000001000000',
                                   'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            elif key == 'INC':
                self.__bin.append({'code': '0111000000100000',
                                   'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            elif key == 'SPA':
                self.__bin.append({'code': '0111000000010000',
                                   'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            elif key == 'SNA':
                self.__bin.append({'code': '0111000000001000',
                                   'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            elif key == 'SZA':
                self.__bin.append({'code': '0111000000000100',
                                   'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            elif key == 'SZE':
                self.__bin.append({'code': '0111000000000010',
                                   'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            elif key == 'HLT':
                self.__bin.append({'code': '0111000000000001',
                                   'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            elif key == 'INP':
                self.__bin.append({'code': '1111100000000000',
                                   'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            elif key == 'OUT':
                self.__bin.append({'code': '1111010000000000',
                                   'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            elif key == 'SKI':
                self.__bin.append({'code': '1111001000000000',
                                   'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            elif key == 'SKO':
                self.__bin.append({'code': '1111000100000000',
                                   'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            elif key == 'ION':
                self.__bin.append({'code': '1111000010000000',
                                   'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            elif key == 'IOF':
                self.__bin.append({'code': '1111000001000000',
                                   'address': str(bin(self.__lc)[2:].zfill(12))})
                self.__lc += 1
            elif key == 'label':
                self.__lc += 1
            elif key == 'Empty':
                pass
            else:
                raise Exception('No keyword found in a line.')
    def find_keyword(self, string):
        """
        Find if a line have a keyword
        """
        reg2 = r"\s*(\w+)\s*,\s*\n"
        for i in self.keyword:
            pattern = re.compile(i)
            pattern2 = re.compile(reg2)
            rans = re.findall(pattern, string)
            rans2 = re.findall(pattern2, string)
            if len(rans):
                return i
            elif len(rans2):
                return 'label'
            elif string == '\n':
                return 'Empty'
        return None
    def bin_to_hex(self):
        """
        Make hex from bin
        """
        for i in self.__bin:
            self.__hex.append({'code': self.hex_maker(i['code'][0:4])+
                                       self.hex_maker(i['code'][4:8])+
                                       self.hex_maker(i['code'][8:12])+
                                       self.hex_maker(i['code'][12:16]),
                               'address': self.hex_maker(i['address'][0:4])+
                                          self.hex_maker(i['address'][4:8])+
                                          self.hex_maker(i['address'][8:12])})
    def hex_maker(self, string):
        """
        change bin to hex
        """
        if string == '0000':
            return '0'
        elif string == '0001':
            return '1'
        elif string == '0010':
            return '2'
        elif string == '0011':
            return '3'
        elif string == '0100':
            return '4'
        elif string == '0101':
            return '5'
        elif string == '0110':
            return '6'
        elif string == '0111':
            return '7'
        elif string == '1000':
            return '8'
        elif string == '1001':
            return '9'
        elif string == '1010':
            return 'a'
        elif string == '1011':
            return 'b'
        elif string == '1100':
            return 'c'
        elif string == '1101':
            return 'd'
        elif string == '1110':
            return 'e'
        elif string == '1111':
            return 'f'
        else:
            raise Exception('No  hex  digit.')
    def output_maker(self):
        """
        Make output
        """
        for i in self.__bin:
            self.__bin_string.append(i['address']+':    '+i['code']+'\n')
        for i in self.__hex:
            self.__hex_string.append(i['address']+':    '+i['code']+'\n')
        with open(self.__output_file_name+'_bin', 'w') as file:
            file.writelines(self.__bin_string)
        with open(self.__output_file_name+'_hex', 'w') as file:
            file.writelines(self.__hex_string)
if  __name__ == '__main__':
    try:
        MI = MyInterpreter()
        MI.token_table()
        MI.txt_to_hex()
        MI.bin_to_hex()
        MI.output_maker()
    except Exception as error:
        print(error)
        '''print(e.args)
        print(e.message)'''
